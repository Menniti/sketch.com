package main

import (
	"sketch.com/config"
	"sketch.com/postgresql"
	"sketch.com/rest"
)

func main() {
	postgresql.Migrate()
	r := rest.SetupRouter()
	r.Run(":" + config.API_SERVICE_PORT)
}
