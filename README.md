SKETCH CHALLENGE

Prerequisits:

- go 1.17

- Docker version 20.10.10


1 - Setup the enviroment
```
go mod tidy
```

2 - Run docker-compose
```
docker-compose up
```

3 - Run tests
```
go test -v ./...
```

4 - Run Golang App
```
go run main.go
```

5 - Sample Browser Request
```
http://localhost:9090/draw?pictures=[[%2214,0%22,7,6,%22%22,%22.%22],[%2215,0%22,4,2,%22O%22,%22.%22]]
```

6 - PG ADMIN
```
http://localhost:16543

EMAIL: luiz@eu.com
PASSWORD: 123123

*config on docker-compose
```