package rest_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"sketch.com/rest"
	"sketch.com/utils"
)

func TestPictureApi(t *testing.T) {
	r := rest.SetupRouter()
	r.GET("/test/draw", rest.HandlerDraw)

	//req, _ := http.NewRequest("GET", "/test/draw", nil)
	querystring := `[["14,0",2,4,"K",""]]`
	path := fmt.Sprintf("/test/draw?pictures=%s", querystring)
	w := utils.PerformRequest(r, "GET", path)
	response := make(map[string]string)
	err := json.Unmarshal(w.Body.Bytes(), &response)

	expected := make(map[string]string)
	expected["hash_picture"] = "117a7f03a15a009cc2c7a9c1c72a4afb7410ee35de64e485ab84f5dd95b4ca5c"
	expected["picture"] = "              KKK\n              K K\n              K K\n              K K\n              KKK\n"
	expected["input"] = querystring
	expected["hash_input"] = utils.GenerateHash(querystring)

	assert.Equal(t, expected["hash_picture"], response["hash_picture"])
	assert.Equal(t, expected["picture"], response["picture"])
	assert.Equal(t, expected["input"], response["input"])
	assert.Equal(t, expected["hash_input"], response["hash_input"])
	assert.Equal(t, nil, err)

	querystring = `[["14,8",2,4,"",""]]`
	path = fmt.Sprintf("/test/draw?pictures=%s", querystring)
	w = utils.PerformRequest(r, "GET", path)
	assert.Equal(t, "ERROR: Impossible to get the params, please follow the example below \n /draw?pictures=[[\"14,0\",7,6,\"\",\".\"],[\"0,3\",8,4,\"O\",\"\"],[\"5,5\",5,3,\"X\",\"X\"]]", w.Body.String())
}
