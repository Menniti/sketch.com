package rest

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"sketch.com/draw"
	"sketch.com/utils"
)

var HandlerDraw = func(c *gin.Context) {
	params := c.Request.URL.Query()
	pictures_str := params.Get("pictures")
	pictures_str = strings.ReplaceAll(pictures_str, " ", "")
	pictures_str = strings.ReplaceAll(pictures_str, "%20", "")
	ArrDraw, err := draw.GetSliceOfDraws(pictures_str)
	if err != nil {
		example := `/draw?pictures=[["14,0",7,6,"","."],["0,3",8,4,"O",""],["5,5",5,3,"X","X"]]`
		c.String(http.StatusBadRequest, "ERROR: Impossible to get the params, please follow the example below \n %s", example)
		return
	}
	hash_input := utils.GenerateHash(pictures_str)
	dbPicture := draw.GetCanvasByHashInput(hash_input)
	if dbPicture != nil {
		log.Println(dbPicture)
		c.JSON(http.StatusOK, dbPicture)
		return
	}
	picture, err := draw.DrawPicture(ArrDraw)
	if err != nil {
		example := `/draw?pictures=[["14,0",7,6,"","."],["0,3",8,4,"O",""],["5,5",5,3,"X","X"]]`
		log.Printf("ERROR: Impossible to get the params, please follow the example below \n %s", example)
		c.String(http.StatusBadRequest, "ERROR: Impossible to get the params, please follow the example below \n %s", example)
		return
	}
	picture.HashInput = hash_input
	picture.Input = pictures_str
	draw.InsertCanvasPicture(*picture)
	c.JSON(http.StatusOK, picture)
}

func SetupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	}))

	r.GET("/healthcheck", func(c *gin.Context) {
		c.String(http.StatusOK, "healthcheck")
	})

	r.GET("/draw", HandlerDraw)

	return r
}
