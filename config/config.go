package config

import (
	"log"
	"os"
)

var API_SERVICE_PORT string

var POSTGRES_HOST string
var POSTGRES_PORT string
var POSTGRES_USER string
var POSTGRES_PASSWORD string
var POSTGRES_DB string

func init() {
	log.Println("Config package initialized")
	API_SERVICE_PORT = os.Getenv("API_SERVICE_PORT")
	POSTGRES_HOST = os.Getenv("POSTGRES_HOST")
	POSTGRES_PORT = os.Getenv("POSTGRES_PORT")
	POSTGRES_USER = os.Getenv("POSTGRES_USER")
	POSTGRES_PASSWORD = os.Getenv("POSTGRES_PASSWORD")
	POSTGRES_DB = os.Getenv("POSTGRES_DB")

	if API_SERVICE_PORT == "" && POSTGRES_HOST == "" && POSTGRES_PORT == "" && POSTGRES_USER == "" && POSTGRES_PASSWORD == "" && POSTGRES_DB == "" {
		API_SERVICE_PORT = "9090"
		POSTGRES_HOST = "localhost"
		POSTGRES_PORT = "5432"
		POSTGRES_USER = "postgres"
		POSTGRES_PASSWORD = "postgres"
		POSTGRES_DB = "postgres"
	}
}
