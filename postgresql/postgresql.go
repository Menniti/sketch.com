package postgresql

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"sketch.com/config"
)

var connStr = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", config.POSTGRES_HOST, config.POSTGRES_PORT, config.POSTGRES_USER, config.POSTGRES_PASSWORD, config.POSTGRES_DB)

func ConnectPostgresdb() *sql.DB {
	log.Println(connStr)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		CheckErr(err)
	}

	return db
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func QueryData(query string, db *sql.DB) (*sql.Rows, error) {

	if db == nil {
		db = ConnectPostgresdb()
	}
	defer db.Close()
	log.Println(query)
	rows, err := db.Query(query)
	CheckErr(err)

	return rows, err

}

func InsertData(query string, db *sql.DB) {

	if db == nil {
		db = ConnectPostgresdb()
	}
	defer db.Close()
	log.Println(query)
	_, err := db.Exec(query)
	CheckErr(err)

}

func QueryRow(query string, db *sql.DB) *sql.Row {

	if db == nil {
		db = ConnectPostgresdb()
	}
	defer db.Close()
	log.Println(query)
	row := db.QueryRow(query)

	return row

}

func getQueryCreatePictureTable() string {
	return `CREATE TABLE IF NOT EXISTS picture (
		id serial,
		input varchar(65535) NOT NULL,
		hash_input varchar(65) NOT NULL,
		picture varchar(65535) NOT NULL,
		hash_picture varchar(65) NOT NULL,
		UNIQUE(hash_input, hash_picture)
	  )`
}

func Migrate() {
	db := ConnectPostgresdb()
	defer db.Close()
	query := getQueryCreatePictureTable()
	row, err := db.Exec(query)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	log.Println(row)
}
