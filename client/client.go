package client

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"sketch.com/config"
	"sketch.com/draw"
)

func getUrl(path string) string {
	url := config.API_SERVICE_PORT
	url = url + path
	return url
}

func getRequest(path string) ([]byte, error) {
	resp, err := http.Get(path)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		return nil, err
	}
	return body, nil
}

func GetDraw(params string) (*draw.Picture, error) {
	var pic draw.Picture
	if params == "" {
		log.Printf("Error reading params: %v", nil)
		return nil, errors.New("ERROR: Please provide params to get it")
	}
	url := getUrl("/draw?pictures=")
	path := fmt.Sprintf(`%s%s`, url, params)
	body, err := getRequest(path)
	if err != nil {
		log.Printf("Error get request body: %v", err)
		return nil, err
	}
	err = json.Unmarshal(body, &pic)
	if err != nil {
		log.Printf("Error unmarshal body: %v", err)
		return nil, err
	}
	return &pic, nil
}
