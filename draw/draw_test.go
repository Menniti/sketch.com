package draw_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"sketch.com/draw"
)

func TestGetRangeSlices(t *testing.T) {
	var drawS draw.Draw
	drawS.PairCoordinates = "14,0"
	drawS.Width = 2
	drawS.Heigth = 4
	drawS.Outline = "K"
	drawS.Fill = ""
	x, y, width, heigth, widthRange, heigthRange, err := draw.GetRangeSlices(drawS)
	assert.Equal(t, 14, x)
	assert.Equal(t, 0, y)
	assert.Equal(t, 2, width)
	assert.Equal(t, 4, heigth)
	assert.Equal(t, []int{14, 15, 16}, widthRange)
	assert.Equal(t, []int{0, 1, 2, 3, 4}, heigthRange)
	assert.Equal(t, nil, err)

	var drawS2 draw.Draw
	drawS2.PairCoordinates = "2,9"
	drawS2.Width = 8
	drawS2.Heigth = 9
	drawS2.Outline = ""
	drawS2.Fill = "J"
	x, y, width, heigth, widthRange, heigthRange, err = draw.GetRangeSlices(drawS2)
	assert.Equal(t, 2, x)
	assert.Equal(t, 9, y)
	assert.Equal(t, 8, width)
	assert.Equal(t, 9, heigth)
	assert.Equal(t, []int{2, 3, 4, 5, 6, 7, 8, 9, 10}, widthRange)
	assert.Equal(t, []int{9, 10, 11, 12, 13, 14, 15, 16, 17, 18}, heigthRange)
	assert.Equal(t, nil, err)

}

func TestDrawRectangle(t *testing.T) {
	var DrawSymbols = make(map[string]string)
	var drawS draw.Draw
	drawS.PairCoordinates = "14,0"
	drawS.Width = 2
	drawS.Heigth = 4
	drawS.Outline = "K"
	drawS.Fill = ""
	DrawSymbols, x, y, width, heigth, _ := draw.DrawRectangle(drawS, DrawSymbols)
	assert.Equal(t, map[string]string(map[string]string{"14,0": "K", "14,1": "K", "14,2": "K", "14,3": "K", "14,4": "K", "15,0": "K", "15,1": "", "15,2": "", "15,3": "", "15,4": "K", "16,0": "K", "16,1": "K", "16,2": "K", "16,3": "K", "16,4": "K"}), DrawSymbols)
	assert.Equal(t, 14, x)
	assert.Equal(t, 0, y)
	assert.Equal(t, 2, width)
	assert.Equal(t, 4, heigth)

	DrawSymbols = make(map[string]string)
	var drawS2 draw.Draw
	drawS2.PairCoordinates = "6,7"
	drawS2.Width = 5
	drawS2.Heigth = 8
	drawS2.Outline = "L"
	drawS2.Fill = "J"
	DrawSymbols, x, y, width, heigth, err := draw.DrawRectangle(drawS2, DrawSymbols)
	assert.Equal(t, map[string]string{"10,10": "J", "10,11": "J", "10,12": "J", "10,13": "J", "10,14": "J", "10,15": "L", "10,7": "L", "10,8": "J", "10,9": "J", "11,10": "L", "11,11": "L", "11,12": "L", "11,13": "L", "11,14": "L", "11,15": "L", "11,7": "L", "11,8": "L", "11,9": "L", "6,10": "L", "6,11": "L", "6,12": "L", "6,13": "L", "6,14": "L", "6,15": "L", "6,7": "L", "6,8": "L", "6,9": "L", "7,10": "J", "7,11": "J", "7,12": "J", "7,13": "J", "7,14": "J", "7,15": "L", "7,7": "L", "7,8": "J", "7,9": "J", "8,10": "J", "8,11": "J", "8,12": "J", "8,13": "J", "8,14": "J", "8,15": "L", "8,7": "L", "8,8": "J", "8,9": "J", "9,10": "J", "9,11": "J", "9,12": "J", "9,13": "J", "9,14": "J", "9,15": "L", "9,7": "L", "9,8": "J", "9,9": "J"}, DrawSymbols)
	assert.Equal(t, 6, x)
	assert.Equal(t, 7, y)
	assert.Equal(t, 5, width)
	assert.Equal(t, 8, heigth)
	assert.Equal(t, nil, err)

}

func TestDrawPicture(t *testing.T) {
	var drawS draw.Draw
	drawS.PairCoordinates = "4,0"
	drawS.Width = 6
	drawS.Heigth = 2
	drawS.Outline = "K"
	drawS.Fill = "J"

	var drawS2 draw.Draw
	drawS2.PairCoordinates = "10,15"
	drawS2.Width = 5
	drawS2.Heigth = 3
	drawS2.Outline = "L"
	drawS2.Fill = "P"

	var drawS3 draw.Draw
	drawS3.PairCoordinates = "12,9"
	drawS3.Width = 5
	drawS3.Heigth = 2
	drawS3.Outline = "W"
	drawS3.Fill = "Y"

	var ArrDraw []draw.Draw
	ArrDraw = append(ArrDraw, drawS)
	ArrDraw = append(ArrDraw, drawS2)
	ArrDraw = append(ArrDraw, drawS3)

	picture, err := draw.DrawPicture(ArrDraw)
	assert.Equal(t, &draw.Picture{HashPicture: "9f17aad4aedddd75784ba1c4ca6ae4e008173c0a0f3eeb7b9c9732fd821fc826", Picture: "    KKKKKKK       \n    KJJJJJK       \n    KKKKKKK       \n                  \n                  \n                  \n                  \n                  \n                  \n            WWWWWW\n            WYYYYW\n            WWWWWW\n                  \n                  \n                  \n          LLLLLL  \n          LPPPPL  \n          LPPPPL  \n          LLLLLL  \n"}, picture)
	assert.Equal(t, nil, err)
}

func TestGetSliceOfDraws(t *testing.T) {
	picture_str := `[["14,0",7,6,"","."],["0,3",8,4,"O",""],["5,5",5,3,"J","X"]]`
	draws, err := draw.GetSliceOfDraws(picture_str)
	assert.Equal(t, []draw.Draw{{PairCoordinates: "14,0", Width: 7, Heigth: 6, Outline: "", Fill: "."}, {PairCoordinates: "0,3", Width: 8, Heigth: 4, Outline: "O", Fill: ""}, {PairCoordinates: "5,5", Width: 5, Heigth: 3, Outline: "J", Fill: "X"}}, draws)
	assert.Equal(t, nil, err)
}
