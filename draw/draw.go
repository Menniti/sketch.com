package draw

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"

	"sketch.com/utils"
)

type Draw struct {
	PairCoordinates string
	Width           int
	Heigth          int
	Outline         string
	Fill            string
}

type Picture struct {
	HashPicture string `json:"hash_picture"`
	Picture     string `json:"picture"`
	Input       string `json:"input"`
	HashInput   string `json:"hash_input"`
}

func GetSliceOfDraws(pictures_str string) ([]Draw, error) {
	var IPicture []interface{}
	var ArrDraw []Draw
	pictures_bytes := []byte(pictures_str)
	err := json.Unmarshal(pictures_bytes, &IPicture)
	if err != nil {
		example := `/draw?pictures=[["14,0",7,6,"","."],["0,3",8,4,"O",""],["5,5",5,3,"X","X"]]`
		log.Println(err)
		log.Printf("ERROR: Impossible to get the params, please follow the example below \n %s", example)
		return nil, err
	}

	for _, pic := range IPicture {
		pic_str := strings.Split(fmt.Sprintf("%v", pic), " ")
		width, err := strconv.Atoi(pic_str[1])
		if err != nil {
			log.Println(err)
			break
		}
		heigth, err := strconv.Atoi(pic_str[2])
		if err != nil {
			log.Println(err)
			break
		}
		fill := pic_str[4][:1]
		if fill == "]" {
			fill = ""
		}

		ArrDraw = append(ArrDraw, Draw{
			PairCoordinates: pic_str[0][1:],
			Width:           width,
			Heigth:          heigth,
			Outline:         pic_str[3],
			Fill:            fill,
		})
	}
	return ArrDraw, nil
}

func GetRangeSlices(draw Draw) (x int, y int, width int, heigth int, widthRange []int, heigthRange []int, err error) {
	width = draw.Width
	heigth = draw.Heigth
	if width < 0 {
		log.Println(err)
		return 0, 0, 0, 0, nil, nil, err
	}
	if heigth < 0 {
		log.Println(err)
		return 0, 0, 0, 0, nil, nil, err
	}

	coordinate := strings.Split(draw.PairCoordinates, ",")
	x, err = strconv.Atoi(coordinate[0])
	if err != nil {
		log.Println(err)
		return 0, 0, 0, 0, nil, nil, err
	}
	y, err = strconv.Atoi(coordinate[1])
	if err != nil {
		log.Println(err)
		return 0, 0, 0, 0, nil, nil, err
	}
	widthRange, err = utils.NewSlice(x, width+1, 1)
	if err != nil {
		log.Println(err)
		return 0, 0, 0, 0, nil, nil, err
	}
	heigthRange, err = utils.NewSlice(y, heigth+1, 1)
	if err != nil {
		log.Println(err)
		return 0, 0, 0, 0, nil, nil, err
	}
	return x, y, width, heigth, widthRange, heigthRange, nil
}

func DrawRectangle(draw Draw, drawSymbols map[string]string) (map[string]string, int, int, int, int, error) {

	outline := draw.Outline
	fill := draw.Fill
	x, y, width, heigth, widthRange, heigthRange, err := GetRangeSlices(draw)
	if err != nil {
		log.Println("ERROR: Impossible to Draw a Rectangle")
		return nil, 0, 0, 0, 0, err
	}
	for _, h := range heigthRange {
		for _, w := range widthRange {
			key := fmt.Sprintf("%d,%d", w, h)
			switch {
			case (w == x):
				drawSymbols[key] = outline
			case (h == y):
				drawSymbols[key] = outline
			case (w == x+width):
				drawSymbols[key] = outline
			case (h == y+heigth):
				drawSymbols[key] = outline
			default:
				drawSymbols[key] = fill
			}
		}
	}
	return drawSymbols, x, y, width, heigth, nil
}

func DrawPicture(arrDraw []Draw) (*Picture, error) {
	picture := ""
	DrawSymbols := make(map[string]string)
	maxWidth := 0
	maxHeigth := 0
	x := 0
	y := 0
	width := 0
	heigth := 0
	err := errors.New("")
	for _, value := range arrDraw {
		outline := value.Outline
		fill := value.Fill
		if outline == "" && fill == "" {
			err := errors.New("ERROR: You must have at least one outline or fill")
			log.Println(err)
			return nil, err
		}
		DrawSymbols, x, y, width, heigth, err = DrawRectangle(value, DrawSymbols)

		if err != nil {
			err := errors.New("ERROR: Impossible to Draw a Rectangle")
			log.Println(err)
			return nil, err
		}
		if x+width > maxWidth {
			maxWidth = x + width
		}
		if y+heigth > maxHeigth {
			maxHeigth = y + heigth
		}
	}
	widthRange, err := utils.NewSlice(0, maxWidth+1, 1)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	heigthRange, err := utils.NewSlice(0, maxHeigth+1, 1)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	for _, h := range heigthRange {
		for _, w := range widthRange {
			key := fmt.Sprintf("%d,%d", w, h)
			value := DrawSymbols[key]
			if value == "" {
				value = " "
			}
			picture = picture + value
		}
		picture = picture + "\n"
	}
	hash := utils.GenerateHash(picture)
	return &Picture{Picture: picture, HashPicture: hash}, nil
}
