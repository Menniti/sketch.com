package draw_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"sketch.com/draw"
)

func TestQueryInsertPicture(t *testing.T) {
	picture := draw.Picture{HashPicture: "9f17aad4aedddd75784ba1c4ca6ae4e008173c0a0f3eeb7b9c9732fd821fc826", Picture: "    KKKKKKK", Input: "[]", HashInput: "f3eeb7b9c9732fd821fc826"}
	query := draw.GetQueryInsertPicture(picture)
	expected := "INSERT INTO picture (input, hash_input, picture, hash_picture) VALUES ('[]','f3eeb7b9c9732fd821fc826','    KKKKKKK','9f17aad4aedddd75784ba1c4ca6ae4e008173c0a0f3eeb7b9c9732fd821fc826')"
	assert.Equal(t, expected, query)
}

func TestQuerySelectPicture(t *testing.T) {
	hash := "f3eeb7b9c9732fd821fc826"
	expected := "SELECT input,hash_input,picture,hash_picture FROM picture WHERE hash_input = 'f3eeb7b9c9732fd821fc826'"
	query := draw.GetQuerySelectPictureByHash(hash)
	assert.Equal(t, expected, query)
}

// TODO implement test for database mock
