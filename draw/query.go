package draw

import (
	"fmt"

	"sketch.com/postgresql"
)

func GetQueryInsertPicture(picture Picture) string {
	query := fmt.Sprintf("INSERT INTO picture (input, hash_input, picture, hash_picture) VALUES ('%s','%s','%s','%s')", picture.Input, picture.HashInput, picture.Picture, picture.HashPicture)
	return query
}

func InsertCanvasPicture(picture Picture) {
	query := GetQueryInsertPicture(picture)
	postgresql.InsertData(query, nil)
}

func GetQuerySelectPictureByHash(hash string) string {
	query := fmt.Sprintf("SELECT input,hash_input,picture,hash_picture FROM picture WHERE hash_input = '%s'", hash)
	return query
}

func GetCanvasByHashInput(hash string) *Picture {
	var picture Picture
	query := GetQuerySelectPictureByHash(hash)
	row := postgresql.QueryRow(query, nil)
	row.Scan(&picture.Input, &picture.HashInput, &picture.Picture, &picture.HashPicture)
	if (Picture{}) == picture {
		return nil
	}
	return &picture
}
