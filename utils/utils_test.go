package utils_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"sketch.com/utils"
)

func TestNewSlice(t *testing.T) {
	start := 10
	count := 7
	step := 1
	slice, err := utils.NewSlice(start, count, step)
	assert.Equal(t, []int{10, 11, 12, 13, 14, 15, 16}, slice)
	assert.Equal(t, nil, err)

	start = 10
	count = 7
	step = -1
	slice, err = utils.NewSlice(start, count, step)
	assert.Equal(t, []int{10, 9, 8, 7, 6, 5, 4}, slice)
	assert.Equal(t, nil, err)

	start = -10
	count = 2
	step = 2
	slice, err = utils.NewSlice(start, count, step)
	assert.Equal(t, []int{-10, -8}, slice)
	assert.Equal(t, nil, err)

}

func TestGenerateHash(t *testing.T) {
	testString := "which will be the hash for this string?"
	hash := utils.GenerateHash(testString)
	assert.Equal(t, "a725b3e3a42d93c9909cd8203e87d0e26694198efa75970eda42bfd9c0ae02e7", hash)
}
