package utils

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
)

func NewSlice(start, count, step int) ([]int, error) {
	if count <= 0 {
		err := errors.New("ERROR: You must have count => 0")
		return nil, err
	}
	s := make([]int, count)
	for i := range s {
		s[i] = start
		start += step
	}
	return s, nil
}

func GenerateHash(stringTohash string) string {
	h := sha256.New()
	h.Write([]byte(stringTohash))
	hash := fmt.Sprintf("%x", h.Sum(nil))
	return hash
}

func PerformRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}
